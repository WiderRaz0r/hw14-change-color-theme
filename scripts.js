const changeTheme = document.querySelector(`[data-change-theme="dark"]`);
const pageBody = document.querySelector(`.defaultBody`);

let darkTheme

function changeToDarkTheme(){
    pageBody.style.background = `rgb(12,25,39)`;
}

function changeToLightTheme(){
    pageBody.style.background = `rgb(0,123,255)`;
}

changeTheme.addEventListener(`click`, () => {
    if (!darkTheme){
        localStorage.setItem(`darkTheme`, `true`);
        darkTheme = true;
        changeToLightTheme()
    } else {
        localStorage.setItem(`darkTheme`, `false`);
        darkTheme = false;
        changeToDarkTheme()
    }
})

if (localStorage.getItem('darkTheme') === 'true') {
    DarkTheme = true
    changeToDarkTheme()
} else {
    localStorage.setItem('darkTheme', 'false');
    DarkTheme = false
    changeToLightTheme()
}
